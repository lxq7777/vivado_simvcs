PREINSTALL_DIR := ${PWD}

INSTALL_DIR := $(shell cd ~ && pwd)/vivado_simlib
REPLACE_EXNAME := .f


install:
	@echo "Try to install in ${INSTALL_DIR}"
	mkdir -p ${INSTALL_DIR}
	cp ${PREINSTALL_DIR}/* ${INSTALL_DIR} -rf

replace:
	find ${INSTALL_DIR}/secureip -type f -name "*${REPLACE_EXNAME}" | while read file; do \
		sed -i 's%\$$XILINX_VIVADO/data%${INSTALL_DIR}%g' $$file; \
		echo "Replaced in $$file"; \
	done

all: install replace run_test

run_test:
	cd ${INSTALL_DIR}/vcs_test_install && make all

uninstall:
	@echo "Remove ${INSTALL_DIR}"
	@rm -rf ${INSTALL_DIR}