## Xilinx IP for VCS sim install script

### 1.Copy files

copy followings into this root dir
> \$Vivado_home/data/verilog/src/unifast
 \$Vivado_home/data/verilog/src/unimacro
 \$Vivado_home/data/verilog/src/unisims
 \$Vivado_home/data/verilog/src/unisims_dr
 \$Vivado_home/data/verilog/src/glbl.v

### 2.Install and Test

run `make all` to install and run simple vcs test
all files will install in ~/vivado_simlib